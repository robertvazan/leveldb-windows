# LevelDB for Windows (discontinued) #

This used to be my Windows (Visual Studio) port of LevelDB.
It is no longer maintained.
Please use one of the projects below:

* [Official Windows branch from Google (C/C++)](https://github.com/google/leveldb/tree/windows)
* [Updated fork of my LevelDB port for Visual Studio (C++)](https://github.com/ren85/leveldb-windows)
* [LevelDB for Windows and .NET](https://github.com/Reactive-Extensions/LevelDB)
* [LevelDB.Net - C++/CLI port](https://github.com/neo-project/leveldb/tree/cli)
* [leveldb-sharp - .NET/C# wrapper](https://www.meebey.net/projects/leveldb-sharp/)

